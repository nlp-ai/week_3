# coding: utf-8
import argparse
import time
import math
import torch
import os
import torch.onnx
from utils.data_utils import Vocab, Txtfile, Data2tensor, SaveloadHP, seqPAD, PAD
from utils.core_nns import RNNModel
from model import Languagemodel

PAD = u"<PAD>"
UNK = u"<UNK>"
SOS = u"<s>"
EOS = u"</s>"

# Set the random seed manually for reproducibility.
Data2tensor.set_randseed(1234)


def build_args():
	"""
	:return: args
	"""
	parser = argparse.ArgumentParser(description='Language Model')
	parser.add_argument('--train_file', help='Trained file', default="./dataset/train.small.txt", type=str)
	parser.add_argument('--dev_file', help='Trained file', default="./dataset/val.small.txt", type=str)
	parser.add_argument('--test_file', help='Tested file', default="./dataset/test.small.txt", type=str)
	parser.add_argument("--wl_th", type=int, default=-1, help="Word threshold")
	parser.add_argument("--cutoff", type=int, default=1, help="Prune words occurring <= wcutoff")
	parser.add_argument("--se_words", action='store_false', default=True, help="Start-end padding flag at word")
	parser.add_argument("--allow_unk", action='store_false', default=True, help="allow using unknown padding")
	parser.add_argument(
		'--model', type=str, default='RNN_TANH', help='type of recurrent net (RNN_TANH, RNN_RELU, LSTM, GRU)')
	parser.add_argument('--emsize', type=int, default=16, help='size of word embeddings')
	parser.add_argument('--nhid', type=int, default=32, help='number of hidden units per layer')
	parser.add_argument('--nlayers', type=int, default=1, help='number of layers')
	parser.add_argument('--bidirect', action='store_true', default=False, help='bidirectional flag')
	parser.add_argument('--lr', type=float, default=20, help='initial learning rate')
	parser.add_argument('--clip', type=float, default=0.25, help='gradient clipping')
	parser.add_argument('--epochs', type=int, default=8, help='upper epoch limit')
	# batch_size: here, it is the number of sentence being training at the same time
	# The actual value of batch_size = batch_size * bptt
	parser.add_argument('--batch_size', type=int, default=16, metavar='N', help='batch size')
	parser.add_argument('--bptt', type=int, default=32, help='sequence length')
	parser.add_argument('--es', type=int, default=2, help='Early stopping criterion')
	parser.add_argument('--dropout', type=float, default=0.5, help='dropout applied to layers (0 = no dropout)')
	parser.add_argument('--tied', action='store_true', help='tie the word embedding and softmax weights')
	parser.add_argument('--trained_model', type=str, default='./results/lm.m', help='path to save the final model')
	parser.add_argument('--model_args', type=str, default='./results/lm.args', help='path to save the model argument')
	parser.add_argument("--use_cuda", action='store_true', default=False, help="GPUs Flag (default False)")

	arguments = parser.parse_args()
	arguments = Languagemodel.build_data(arguments)

	return arguments


def predict(txt, lm):
	"""
	:param txt: a sentence string
	:param lm: language model object
	:return: a list of possible words
	"""

	# CONVERT TXT TO A LIST OF NUMBERS
	w2i = lm.args.vocab.w2i
	#print("w2i: ", w2i)
	word_list = txt.split()
	word_list_number = []
	for word in word_list:
		if word in w2i:
			word_list_number.append(w2i[word])
		else:
			word_list_number.append(w2i[UNK])
	#print ("word_list_number", word_list_number)

	# CONVERT TO TENSOR
	tensor = Data2tensor.idx2tensor([word_list_number], lm.device)

	# HIDDEN LAYER
	hidden = lm.model.init_hidden(tensor.size(0))

	# PREDICT
	output, hidden = lm.model(tensor, hidden)   # output is a tensor, not a number hmm
	#print("output", output)

	# CONVERT OUTPUT (TENSOR) INTO OUTPUT (NUMBERS)
	label_prob, label_pred = lm.model.inference(output, 1)
	#print("label prediction", label_pred)

	# CONVERT OUTPUT (NUMBERS) INTO OUTPUT (WORDS)
	i2w = lm.args.vocab.i2w
	# print("i2w", i2w)
	word_output = [i2w[sublist[0]] for sublist in label_pred.tolist()[0]]

	return word_output


def load_argument_file(args):
	print("Loading argument file...")

	vocab = Vocab(wl_th=args.wl_th, cutoff=args.cutoff)
	vocab.build([args.train_file, args.dev_file])
	args.vocab = vocab

	SaveloadHP.save(args, args.model_args)  # TODO: load?
	return args


def load_model(args):
	print("\n")
	print("Loading model file...")
	lmdl = Languagemodel(args)
	lmdl.model.load_state_dict(torch.load(lmdl.args.trained_model))
	return lmdl


def rev_gen(lm):
	current = u"<s> you"
	sentence = []
	for i in range(0, 10):
		sentence += [current]
		print(sentence)
		next_word = predict(' '.join(sentence[-5:]), lm)
		print(next_word)
		current = next_word[-1]

		if current == u"</s>":
			break
	return sentence


def wd_pred(current, lm):
	result = predict(current, lm)
	print(result)
	return result[-1]



if __name__ == '__main__':
	# LOAD ARGUMENTS FILE (args lie in args.model_args)
	args = build_args()
	args = load_argument_file(args)

	# LOAD MODEL FILE
	lm = load_model(args)   # Languagemodel object

	rev_gen(lm)

	#print(wd_pred('you was not a', lm))





